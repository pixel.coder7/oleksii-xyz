<center>
<h1>Landing page</h1>
🚀 <i>Production branch is <code>deployed</code> to <a href="https://oleksii.xyz"><b>oleksii.xyz</b></a></i>
</center>

<br>

<img style="border-radius: 12px;" src="preview.gif"></img>

<br>

---

<br>

<center><i>Check out also <a href="https://github.com/oleksiibesida/oleksii.xyz/tree/2020">2020 version</a> branch</i></center>
